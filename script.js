const MATCH_ID = 0;
const MATCH_SEASON = 1;
const MATCH_RESULT = 8;
const MATCH_WINNER = 10;
const MATCH_PLAYER_OF_MATCH = 13;

const DELIVERY_ID = 0;
const DELIVEY_BOWLING_TEAM = 3;
const DELIVERY_BALL = 5;
const DELIVERY_BOWLER = 8;
const DELIVEY_WIDE_RUNS = 10;
const DELIVERY_BYE_RUNS = 11;
const DELIVERY_LEG_BYE_RUNS = 12;
const DELIVERY_NO_BALL_RUNS = 13;
const DELIVERY_EXTRA_RUNS = 16;
const DELIVERY_TOTAL_RUNS = 17;

const matchesInputElement = document.getElementById("matches");
const deliveriesInputElement = document.getElementById("deliveries");
const errorElement = document.getElementById("error");
const successElement = document.getElementById("success");

let idSeasonObject = {};
let successText = [];

matchesInputElement.addEventListener("change", handleFile);
deliveriesInputElement.addEventListener("change", handleFile);

function handleFile(event) {
  let fileReader = new FileReader();
  if (event.target.id === "matches") {
    fileReader.addEventListener("load", getMatchesData);
  } else {
    fileReader.addEventListener("load", getDeliveriesData);
  }
  fileReader.readAsText(this.files[0]);
}

function getMatchesData() {
  let matches = [];
  let lines = this.result.split(/\r?\n/);

  for (let i = 1; i < lines.length - 1; i++) {
    let columns = lines[i].split(",");
    let match = new Match(
      columns[MATCH_ID],
      columns[MATCH_SEASON],
      columns[MATCH_RESULT],
      columns[MATCH_WINNER],
      columns[MATCH_PLAYER_OF_MATCH]
    );
    matches.push(match);
    idSeasonObject[match.id] = match.season;
  }

  errorElement.classList.add("not-visible");
  successElement.classList.remove("not-visible");
  successElement.innerText = "Now upload deliveries file";
  findMatchesPlayedPerSeason(matches);
  findNumberOfWinsPerTeam(matches);
  findAwardsOfTop10Players(matches);
}

function getDeliveriesData() {
  let deliveries = [];

  if (Object.keys(idSeasonObject).length === 0) {
    errorElement.classList.remove("not-visible");
    successElement.classList.add("not-visible");
  } else {
    errorElement.classList.add("not-visible");
    let lines = this.result.split(/\r?\n/);

    for (let i = 1; i < lines.length - 1; i++) {
      let columns = lines[i].split(",");
      let delivery = new Delivery(
        columns[DELIVERY_ID],
        columns[DELIVEY_BOWLING_TEAM],
        columns[DELIVERY_BALL],
        columns[DELIVERY_BOWLER],
        columns[DELIVEY_WIDE_RUNS],
        columns[DELIVERY_BYE_RUNS],
        columns[DELIVERY_LEG_BYE_RUNS],
        columns[DELIVERY_NO_BALL_RUNS],
        columns[DELIVERY_EXTRA_RUNS],
        columns[DELIVERY_TOTAL_RUNS]
      );
      deliveries.push(delivery);
    }

    findExtraRunsConcededByEachTeamIn2016(deliveries);
    findTop10EconomicalBowlersIn2015(deliveries);

    successText = successText.join("\n");
    successElement.innerText = successText;
    successElement.classList.remove("not-visible");
  }
}

class Match {
  constructor(id, season, result, winner, playerOfMatch) {
    this.id = id;
    this.season = season;
    if (winner === "Rising Pune Supergiants") {
      winner = "Rising Pune Supergiant";
    }
    this.winner = winner;
    this.result = result;
    this.playerOfMatch = playerOfMatch;
  }
}

class Delivery {
  constructor(
    id,
    bowlingTeam,
    ball,
    bowler,
    wideRuns,
    byeRuns,
    legByeRuns,
    noBallRuns,
    extraRuns,
    totalRuns
  ) {
    this.id = id;
    if (bowlingTeam === "Rising Pune Supergiants") {
      bowlingTeam = "Rising Pune Supergiant";
    }
    this.bowlingTeam = bowlingTeam;
    this.ball = parseInt(ball);
    this.bowler = bowler;
    this.wideRuns = parseInt(wideRuns);
    this.byeRuns = parseInt(byeRuns);
    this.legByeRuns = parseInt(legByeRuns);
    this.noBallRuns = parseInt(noBallRuns);
    this.extraRuns = parseInt(extraRuns);
    this.totalRuns = parseInt(totalRuns);
  }
}

function updateObjectData(object, key, value) {
  if (object.hasOwnProperty(key)) {
    object[key] += value;
  } else {
    object[key] = value;
  }
}

function printObjectData(object, lHead, rHead) {
  successText.push(`${lHead}: ${rHead}`);
  Object.entries(object).forEach(([key, value]) => {
    successText.push(`${key}: ${value}`);
  });
  successText.push("   ============================   ");
}

function findMatchesPlayedPerSeason(matches) {
  let matchesPlayedPerSeason = {};
  matches.forEach((match) => {
    let season = match.season;
    updateObjectData(matchesPlayedPerSeason, season, 1);
  });
  printObjectData(matchesPlayedPerSeason, "YEAR", "NO OF MATCHES");
}

function findNumberOfWinsPerTeam(matches) {
  let numberOfWinsPerTeam = {};
  matches.forEach((match) => {
    let result = match.result;
    if (result === "normal") {
      let winner = match.winner;
      updateObjectData(numberOfWinsPerTeam, winner, 1);
    }
  });
  printObjectData(numberOfWinsPerTeam, "TEAM", "NO OF WINS");
}

function findAwardsOfTop10Players(matches) {
  let playersWithAtLeast1PlayerOfMatch = {};

  matches.forEach((match) => {
    let playerOfMatch = match.playerOfMatch;
    if (playerOfMatch !== "") {
      updateObjectData(playersWithAtLeast1PlayerOfMatch, playerOfMatch, 1);
    }
  });

  let sortedPlayers = Object.entries(playersWithAtLeast1PlayerOfMatch).sort(
    (a, b) => b[1] - a[1]
  );

  successText.push(`PLAYER: NO OF AWARDS`);
  for (let i = 0; i < 10 && i < sortedPlayers.length; i++) {
    successText.push(`${sortedPlayers[i][0]}: ${sortedPlayers[i][1]}`);
  }
  successText.push("   ============================   ");
}

function findExtraRunsConcededByEachTeamIn2016(deliveries) {
  let extraRunsConcededByEachTeamIn2016 = {};

  deliveries.forEach((delivery) => {
    let matchId = delivery.id;
    let season = idSeasonObject[matchId];

    if (season === "2016") {
      let bowlingTeam = delivery.bowlingTeam;
      let extraRuns = delivery.extraRuns;
      updateObjectData(
        extraRunsConcededByEachTeamIn2016,
        bowlingTeam,
        extraRuns
      );
    }
  });

  printObjectData(
    extraRunsConcededByEachTeamIn2016,
    "TEAM",
    "EXTRA RUNS CONCEDED"
  );
}

function findTop10EconomicalBowlersIn2015(deliveries) {
  let runsAndBallsByBowlers2015 = {};
  let economiesOfBowlers2015 = {};

  deliveries.forEach((delivery) => {
    let matchId = delivery.id;
    let season = idSeasonObject[matchId];

    if (season === "2015") {
      let byeRuns = delivery.byeRuns;
      let legByeRuns = delivery.legByeRuns;
      let totalRuns = delivery.totalRuns;
      let wideRuns = delivery.wideRuns;
      let noBallRuns = delivery.noBallRuns;
      let bowler = delivery.bowler;

      let countableRuns = totalRuns - byeRuns - legByeRuns;
      let countableBalls = wideRuns === 0 && noBallRuns === 0 ? 1 : 0;

      if (runsAndBallsByBowlers2015.hasOwnProperty(bowler)) {
        runsAndBallsByBowlers2015[bowler][0] += countableRuns;
        runsAndBallsByBowlers2015[bowler][1] += countableBalls;
      } else {
        runsAndBallsByBowlers2015[bowler] = [countableRuns, countableBalls];
      }
    }
  });

  Object.keys(runsAndBallsByBowlers2015).forEach((key) => {
    let runs = runsAndBallsByBowlers2015[key][0];
    let overs = runsAndBallsByBowlers2015[key][1] / 6.0;
    economiesOfBowlers2015[key] = runs / overs;
  });

  let sortedBowlers = Object.entries(economiesOfBowlers2015).sort(
    (a, b) => a[1] - b[1]
  );

  successText.push(`BOWLER NAME: ECONOMY`);
  for (let i = 0; i < 10 && i < sortedBowlers.length; i++) {
    successText.push(
      `${sortedBowlers[i][0]}: ${sortedBowlers[i][1].toFixed(3)}`
    );
  }
  successText.push("   ============================   ");
}
